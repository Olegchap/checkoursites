var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');
var index = require('./routes/index');
var users = require('./routes/users');
var needle = require('needle');
var tress = require('tress');
var cron = require('node-cron');
var CronJob = require('cron').CronJob;
var nodemailer = require('nodemailer');
var SSH = require('simple-ssh');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

//parse our sites
var messageCode = '';
function getSites(){
    var urlErrors  = 0;
    var message = '';
    var q = tress(function (url, callback) {
    needle.get(url, function(error, response) {

        if (!error && response.statusCode == 200){
            // console.log(url+response.statusCode);
        }

        else{
            urlErrors = 1;
            message  += url;
         /// console.log("!!!"+url+response.statusCode);
        }
        callback();
      });
    },50);
    var arrUrl = config.urls.split(',');
    for (var i=0;i<arrUrl.length;i++){
        q.push(arrUrl[i]);
    }
    q.drain = function(){
        if (urlErrors != 0){
            urlErrors = 0;
           // console.log(message);
            messageCode = 1;
            sendMail(message,messageCode);
            restartProcess();
        }
    };
};
//send mail
function sendMail (message,code){
    var transporter = nodemailer.createTransport(config.smtp);

// setup e-mail data with unicode symbols
    if (code == 1 )
    var mailOptions = {
        from: '"check our sites" <www@a3boot.com.ua>', // sender address
        to: config.emails, // list of receivers
        subject: 'OOPS some sites are failed', // Subject line
        text: 'this sites have status difficult then 200: '+message, // plaintext body
        html: '<b>this sites have status difficult then 200:</b>'+message // html body
    };
else {
        var mailOptions = {
            from: '"check our sites" <www@a3boot.com.ua>', // sender address
            to: config.emails, // list of receivers
            subject: 'check our sites work hard', // Subject line
            text: 'check our sites work hard '+message, // plaintext body
            html: '<b>check our sites work hard</b>'+message // html body
        };
    }
// send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
};
// restart process
function restartProcess(){
    var ssh = new SSH({
        host: config.server.host,
        user: config.server.user,
        pass: config.server.password
    });
    ssh
        .exec(config.server.comand1, {
            pty: true,
            out: console.log.bind(console)
        })
        .exec(config.server.comand2, {
            pty: true,
            out: console.log.bind(console)
        }).start();
    ssh.on('error', function(err) {
        console.log('Oops, something went wrong.');
        console.log(err);
        ssh.end();
    });

}
var starCount = '';
var count = 0;

//change timer to *
function timerCount(timer,callback){

if (timer >=  86400 && timer <=  2419200){
    count = (timer/24)/3600 >> 0;
    starCount =  '00 00 00 */'+count+' * *';
    callback();
}
    else if (timer >=  3600 && timer <  86400){
        count = timer/3600 >> 0;
        starCount =  '00 00 */'+count+' * * *';
        callback();
    }
    else if (timer >=  60 && timer <  3600){
    count = timer/60 >> 0;
    starCount =  '00 */'+count+' * * * *';
    callback();
}
else if (timer >=  1 && timer <  60){
    count = timer;
    starCount =  '*/'+count+' * * * * *';
    callback();
}
else{
    starCount = '00 00 00 * * *';
    callback();
}
}
//check sites with cron timer
function check(){
    console.log(starCount) ;
    new CronJob(starCount, function() {
        getSites();
    }, null, true);

}
//if program work
function allRight(){

    new CronJob(starCount, function() {
        messageCode = 2;
        message = '';
        sendMail(message, messageCode);
    }, null, true);
}
timerCount(config.timer,check);
timerCount(config.workTimeTimer,allRight);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
