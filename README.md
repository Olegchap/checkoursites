TO RUN SERVER LOCALLY YOU NEED:

Define node.js version number. ( in command line: node -v )

install NODE.JS version >= 4.4.7 on your machine (https://nodejs.org/en/download/)

go to root directory (where the 'package.json' file located)

run command "npm install" to install all dependencies

create file "config.js" in your root folder, and copy conents of the file "config.example" in your file "config.js"

change parse urls, timer (in seconds), emails (with ","), smtp in format "smtps:/email%40gmail.com:pass@smtp.gmail.com"

if you do this changes at first time read "https://community.nodemailer.com/using-gmail/" and configure your Gmail account
 
change ssh configs ang commands in config.server settings

change work time timer to get messages about program 

run command "npm run start" to run server on localhost:8080 (see "config to change port number ")